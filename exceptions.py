# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.


class LoginException(Exception):
    pass


class NotFound(Exception):
    pass
